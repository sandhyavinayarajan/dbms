var emp_module = angular.module('emp_module',[]);

emp_module.controller('emp_controller',function($scope){
 
  $scope.empdat = [{name : 'Rahul', age : 20, city : 'Delhi'},
                   {name : 'Rohan', age : 30, city : 'Delhi'},
                   {name : 'Varun', age : 20, city : 'Chennai'},
                   {name : 'Rahul', age : 10, city : 'Delhi'},
                   {name : 'Rahul', age : 50, city : 'Mumbai'}];

  $scope.temp;

  $scope.f1 = function(x){
    
    $scope.temp = x;

  }
});