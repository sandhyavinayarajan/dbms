package web1;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class StudentDao implements InfStudentDao {

	@Override
	public void getStudent(StudentVo obj) throws Exception {
		Connection co = DBConnect.getConnection();
		String sql = "select adm_no, f_name, l_name, to_char(dob,'dd-mon-yyyy') as dob1, decode(gender, 'M' , 'Male', 'Female') as gender1 , roll_no from st_details where adm_no = ?";
		PreparedStatement ps = co.prepareStatement(sql);
		ps.setString(1, obj.getAdm_no1());
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			// Object - Relation Mapping
			obj.setF_name1(rs.getString(2));
			obj.setL_name1(rs.getString(3));
			obj.setDob1(rs.getString(4));
			obj.setGender1(rs.getString(5));
			obj.setRoll_no1(rs.getString(6));

		} else {
			throw new Exception("Id does not exist");
		}

	}

}
